using DiLogAnalyzer;

namespace TestProject1;

[TestClass]
public class LogAnalyzerTests
{
  [TestMethod]
  public void LoadAndAnalyze()
  {
    LogReader logReader = new LogReader();
    IEnumerable<LogRecord> readedData1 = logReader.ReadLogFromFile("CIA25W10.Client.Host.2023-01-29.log");
    IEnumerable<LogRecord> readedData2 = logReader.ReadLogFromFile("CIA25W10.WebServer.2023-02-12.log");

    LogAnalyzer logAnalyzer = new LogAnalyzer();
    logAnalyzer.AddRecords(readedData1);
    logAnalyzer.AddRecords(readedData2);

    //Группировка по имени
    var groupedByLg = logAnalyzer.GetRecords().GroupBy(x=>x.lg)
      .OrderByDescending(x=>x.Count())
      .ToArray();

    //Самое большое 
    var mostFrequentEvent = groupedByLg.First().Key;

    DateTime from = DateTime.Parse("2023-02-12 08:55:39");
    DateTime to = DateTime.Parse("2023-02-12 10:57:49");
    
    //Выборка по дате
    var spliceDate = logAnalyzer.GetRecords(x => DateTime.Parse(x.t) >= from && DateTime.Parse(x.t) <= to);
  }
}