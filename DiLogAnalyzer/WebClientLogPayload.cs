﻿namespace DiLogAnalyzer;

/// <summary>
/// Доп нагрузка записи лога для журналов web-клиентов
/// </summary>
public class WebClientCust
{
  public string HealthCheck { get; set; }
  public string Status { get; set; }
  public WebClientData Data { get; set; }
}

/// <summary>
/// Доп данные записи лога для журналов web-клиентов
/// </summary>
public class WebClientData
{
  public int PrivateMB { get; set; }
  public int WorkingSetMB { get; set; }
  public int DegradedThresholdWorkingSetMB { get; set; }
  public int MaxThresholdWorkingSetMB { get; set; }
  public int FreeRamMB { get; set; }
  public int TotalRamMB { get; set; }
  public int PageFileMB { get; set; }
}