﻿using System.Text.Json.Nodes;

namespace DiLogAnalyzer;

/// <summary>
/// Строка лога без дополнительной нагрузки
/// </summary>
public class LogRecord
{
  public string t { get; set; }
  public string pid { get; set; }
  public string l { get; set; }
  public string lg { get; set; }
  public JsonObject cust { get; set; }
  public string tn { get; set; }
  public string v { get; set; }
}