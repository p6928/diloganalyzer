﻿using System.Collections.Concurrent;
using System.Text.Json.Nodes;

namespace DiLogAnalyzer;

/// <summary>
/// Анализатор логов
/// </summary>
public class LogAnalyzer
{
  /// <summary>
  /// Хранилище записей логов
  /// </summary>
  private List<LogRecord> logs = new List<LogRecord>();

  /// <summary>
  /// локер для обеспечения потокобезопасности
  /// </summary>
  private ReaderWriterLockSlim _locker = new ReaderWriterLockSlim();

  /// <summary>
  /// Добавить записи в анализатор
  /// </summary>
  /// <param name="logRecords">Множество записей лога</param>
  public void AddRecords(IEnumerable<LogRecord> logRecords)
  {
    try
    {
      _locker.EnterWriteLock();
      logs.AddRange(logRecords);
    }
    finally
    {
      _locker.ExitWriteLock();
    }
  }

  /// <summary>
  /// Извлечь записи из анализатора
  /// </summary>
  /// <param name="filter">Функция фильтрации</param>
  /// <returns></returns>
  public LogRecord[] GetRecords(Func<LogRecord, bool> filter=null)
  {
    try
    {
      _locker.EnterReadLock();
      if (filter is null) return logs.ToArray();
      else return logs.Where(filter).ToArray();
    }
    finally
    {
      _locker.ExitReadLock();
    }
  }
}