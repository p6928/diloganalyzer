﻿namespace DiLogAnalyzer;

/// <summary>
/// Читайка/парсилка логов из файла
/// </summary>
public class LogReader
{
  /// <summary>
  /// Прочитать лог из файла
  /// </summary>
  /// <param name="filename">Имя файла, откуда читать</param>
  /// <returns></returns>
  public IEnumerable<LogRecord> ReadLogFromFile(string filename)
  {
    var data = System.IO.File.ReadAllText(filename);
    var lines = data.Split('\n',StringSplitOptions.RemoveEmptyEntries);
    List<LogRecord> recs = new List<LogRecord>(lines.Length);
    foreach (var l in lines)
    {
      var logRecord = System.Text.Json.JsonSerializer.Deserialize<LogRecord>(l);
      if (logRecord is null) continue;
      recs.Add(logRecord);
    }
    return recs;
  }
}